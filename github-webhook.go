package githubwebhook

import (
	"crypto/hmac"
	"crypto/sha1"
	"encoding/hex"
	"fmt"
	"io/ioutil"
	"net/http"
	"strings"
)

var githubHeaders = [3]string{
	"X-GitHub-Delivery",
	"X-GitHub-Event",
	"X-Hub-Signature",
}

// IsGithubRequest tests if request is coming from github
func IsGithubRequest(r *http.Request) error {
	for _, header := range githubHeaders {
		if r.Header.Get(header) == "" {
			return fmt.Errorf("Missing header %s", header)
		}
	}

	if !strings.HasPrefix(r.Header.Get("User-Agent"), "GitHub-Hookshot") {
		return fmt.Errorf("Unexpected User-Agent %s", r.Header.Get("User-Agent"))
	}

	return nil
}

// IsValidGithubRequest checks if the request body signature is valid
func IsValidGithubRequest(secret string, r *http.Request) error {
	signature := r.Header.Get("X-Hub-Signature")

	if signature == "" {
		return fmt.Errorf("Invalid signature")
	}

	signatureSplit := strings.SplitAfter(signature, "=")

	var signatureValue string

	if len(signatureSplit) > 1 {
		signatureValue = signatureSplit[1]
	} else {
		signatureValue = signatureSplit[0]
	}

	body, err := ioutil.ReadAll(r.Body)

	if err != nil {
		return err
	}

	mac := hmac.New(sha1.New, []byte(secret))
	mac.Write(body)
	expectedMAC := mac.Sum(nil)
	signatureMac, err := hex.DecodeString(signatureValue)

	if err != nil {
		return err
	}

	if hmac.Equal(signatureMac, expectedMAC) {
		return nil
	}

	return fmt.Errorf("Invalid signature %s, %s %s", secret, signatureValue, hex.EncodeToString(expectedMAC))
}
