package githubwebhook

import (
	"bytes"
	"crypto/hmac"
	"crypto/sha1"
	"encoding/hex"
	"fmt"
	"io/ioutil"
	"net/http"
	"testing"
)

func createHeader(headers map[string]string) http.Header {
	header := http.Header{}

	for key, value := range headers {
		header.Set(key, value)
	}

	return header
}

func createSignature(secret, body string) string {
	// Create a new HMAC by defining the hash type and the key (as byte array)
	h := hmac.New(sha1.New, []byte(secret))

	// Write Data to it
	h.Write([]byte(body))

	// Get result and encode as hexadecimal string
	sha := hex.EncodeToString(h.Sum(nil))

	return sha
}

func addBody(body string, r *http.Request) *http.Request {
	r.Body = ioutil.NopCloser(bytes.NewReader([]byte(body)))
	return r
}

func TestIsGithubRequest(t *testing.T) {
	requests := []struct {
		Expected bool
		Request  *http.Request
	}{
		{false, &http.Request{}},
		{true, &http.Request{
			Header: createHeader(map[string]string{
				"X-GitHub-Delivery": "72d3162e-cc78-11e3-81ab-4c9367dc0958",
				"X-GitHub-Event":    "ping",
				"X-Hub-Signature":   "sha1=7d38cdd689735b008b3c702edd92eea23791c5f6",
				"User-Agent":        "GitHub-Hookshot/044aadd",
			}),
		}},
	}

	for _, testCase := range requests {
		if ret := IsGithubRequest(testCase.Request); (ret != nil) == testCase.Expected {
			t.Errorf("Request should be %t, %s", ret == nil, ret)
		}
	}
}

func TestIsValidGithubRequest(t *testing.T) {
	badSecret := "badSecret"
	goodSecret := "goodSecret"
	requests := []struct {
		Expected bool
		Secret   string
		Request  *http.Request
	}{
		{false, "", addBody("", &http.Request{})},
		{false, goodSecret, addBody("foobar", &http.Request{
			Header: createHeader(map[string]string{
				"X-Hub-Signature": fmt.Sprintf("sha1=%s", createSignature(badSecret, "foobar")),
			}),
		})},
		{true, goodSecret, addBody("foobar", &http.Request{
			Header: createHeader(map[string]string{
				"X-Hub-Signature": fmt.Sprintf("sha1=%s", createSignature(goodSecret, "foobar")),
			}),
		})},
	}

	for _, testCase := range requests {
		if ret := IsValidGithubRequest(testCase.Secret, testCase.Request); (ret != nil) == testCase.Expected {
			t.Errorf("Request should be %t, %s", ret == nil, ret)
		}
	}
}
